﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace ReaderBook.Database
{
    public class VendaDatabase
    {
        public void InserirVendas(Models.ModelVendas venda)
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"insert into tb_vendas(ds_cliente,dia,qtd_vendas,total,id_livro)
                                                  values(@ds_cliente,@dia,@qtd_vendas,@total,@id_livro)";

            comand.Parameters.Add(new MySqlParameter("ds_cliente", venda.cliente));
            comand.Parameters.Add(new MySqlParameter("dia", venda.dia));
            comand.Parameters.Add(new MySqlParameter("qtd_vendas", venda.qtd_vendas));
            comand.Parameters.Add(new MySqlParameter("total", venda.valor));
            comand.Parameters.Add(new MySqlParameter("id_livro", venda.idLivro));

            comand.ExecuteNonQuery();
            con.Close();

        }

        public List<Models.ModelVendasRelacional> ListarVendas()
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"select tb_vendas.*,tb_livros.ds_titulo from tb_livros join tb_vendas
                                                           on tb_livros.id_livros = tb_vendas.id_livro";

            MySqlDataReader reader = comand.ExecuteReader();
            List<Models.ModelVendasRelacional> lista = new List<Models.ModelVendasRelacional>();

            while (reader.Read())
            {
                Models.ModelVendasRelacional vendaR = new Models.ModelVendasRelacional();

                vendaR.idvendas = Convert.ToInt32(reader["id_vendas"]);
                vendaR.cliente = Convert.ToString(reader["ds_cliente"]);
                vendaR.dia = Convert.ToDateTime(reader["dia"]);
                vendaR.qtd_vendas = Convert.ToInt32(reader["qtd_vendas"]);
                vendaR.valor = Convert.ToDecimal(reader["total"]);
                vendaR.titulo = Convert.ToString(reader["id_livro"]);


                lista.Add(vendaR);
            }

            con.Close();
            return lista;
   
        }
    }
}