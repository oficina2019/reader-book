﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaderBook.Database
{
    public class LivroDatabase
    {
        public void Inserir(Models.ModelLivros livro)
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"insert into tb_livros(ds_titulo,ds_autor,ds_genero,qtd,preco)
                                                  values(@ds_titulo,@ds_autor,@ds_genero,@qtd,@preco)";

            comand.Parameters.Add(new MySqlParameter("ds_titulo", livro.titulo));
            comand.Parameters.Add(new MySqlParameter("ds_autor", livro.autor));
            comand.Parameters.Add(new MySqlParameter("ds_genero", livro.genero));
            comand.Parameters.Add(new MySqlParameter("qtd", livro.qtd));
            comand.Parameters.Add(new MySqlParameter("preco", livro.preco));

            comand.ExecuteNonQuery();
            con.Close();

        }
        public List<Models.ModelLivros> Listar()
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"select * from tb_livros";

            MySqlDataReader reader = comand.ExecuteReader();
            List<Models.ModelLivros> lista = new List<Models.ModelLivros>();

            while(reader.Read())
            {
                Models.ModelLivros livros = new Models.ModelLivros();
                livros.idLivro = Convert.ToInt32(reader["id_livros"]);
                livros.titulo = Convert.ToString(reader["ds_titulo"]);
                livros.autor = Convert.ToString(reader["ds_autor"]);
                livros.genero = Convert.ToString(reader["ds_genero"]);
                livros.qtd = Convert.ToInt32(reader["qtd"]);
                livros.preco = Convert.ToDecimal(reader["preco"]);

                lista.Add(livros);
            }

            con.Close();
            return lista;

        }

        public Models.ModelLivros FiltrarPorId(int id)
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @" select * from tb_livros where id_livros = @id_livros";

            comand.Parameters.Add(new MySqlParameter("id_livros", id));

            MySqlDataReader reader = comand.ExecuteReader();

            Models.ModelLivros livros = null;

            if (reader.Read())
            {
                livros = new Models.ModelLivros();
                livros.idLivro = Convert.ToInt32(reader["id_livros"]);
                livros.titulo = Convert.ToString(reader["ds_titulo"]);
                livros.autor = Convert.ToString(reader["ds_autor"]);
                livros.genero = Convert.ToString(reader["ds_genero"]);
                livros.qtd = Convert.ToInt32(reader["qtd"]);
                livros.preco = Convert.ToDecimal(reader["preco"]);
            }
            con.Close();
            
            return livros;

        }

        public void Alterar(Models.ModelLivros livros)
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();


            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"  update tb_livros 
                                        set ds_titulo 		= @ds_titulo,
                                            ds_autor 		= @ds_autor,
                                            ds_genero 	= @ds_genero,
                                            qtd 	= @qtd,
                                            preco 	= @preco
                                      where id_livros = @id_livros ";

            comand.Parameters.Add(new MySqlParameter("id_livros", livros.idLivro));
            comand.Parameters.Add(new MySqlParameter("ds_titulo", livros.titulo));
            comand.Parameters.Add(new MySqlParameter("ds_autor", livros.autor));
            comand.Parameters.Add(new MySqlParameter("ds_genero", livros.genero));
            comand.Parameters.Add(new MySqlParameter("qtd", livros.qtd));
            comand.Parameters.Add(new MySqlParameter("preco", livros.preco));


            comand.ExecuteNonQuery();
            con.Close();
        }
        public void Remover(int id)
        {
            MySqlConnection con = new MySqlConnection("server=localhost;database=readerbook;uid=root;");
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"  delete from tb_livros 
                                           where id_livros = @id_livros ";

            comand.Parameters.Add(new MySqlParameter("id_livros", id));

            comand.ExecuteNonQuery();
            con.Close();
        }



    }
}