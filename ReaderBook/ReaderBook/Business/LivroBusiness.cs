﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaderBook.Business
{
    public class LivroBusiness
    {
        Database.LivroDatabase data = new Database.LivroDatabase();
        Database.VendaDatabase vendas = new Database.VendaDatabase();

        public void Inserir(Models.ModelLivros livro)
        {
            data.Inserir(livro);
        }
        public List<Models.ModelLivros> Listar()
        {
           List<Models.ModelLivros> lista = data.Listar();
            return lista;
        }
        public Models.ModelLivros Filtrar(int id)
        {
          Models.ModelLivros livros = data.FiltrarPorId(id);
          return livros;
        }
        public void Alterar(Models.ModelLivros livros)
        {
            data.Alterar(livros);
        }
        public void Deletar(int id)
        {
            data.Remover(id);
        }
        public void InserirVendas(Models.ModelVendas venda)
        {
            vendas.InserirVendas(venda);
        }
        public List<Models.ModelVendasRelacional> ListarVendas()
        {
            List<Models.ModelVendasRelacional> lista = vendas.ListarVendas();
            return lista;
        }
    }
}