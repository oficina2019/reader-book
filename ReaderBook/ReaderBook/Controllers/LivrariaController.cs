﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReaderBook.Controllers
{
    public class LivrariaController : Controller
    {
        // GET: Livraria
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult QuemSomos()
        {
            return View();
        }
        public ActionResult Inserir()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Inserir(Models.ModelLivros livros)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            business.Inserir(livros);

            return View();
        }
        public ActionResult Listar()
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            List<Models.ModelLivros> lista = business.Listar();

            return View(lista);
        }
        public ActionResult InserirVendas()
        {
            return View();
        }
        [HttpPost]
        public ActionResult InserirVendas(Models.ModelVendas venda)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            business.InserirVendas(venda);

            return View();
        }
        public ActionResult ListarVendas()
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            List<Models.ModelVendasRelacional> lista = business.ListarVendas();

            return View(lista);
        }

        public ActionResult Alterar(int id)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            Models.ModelLivros livros = business.Filtrar(id);

            return View(livros);
        }
        [HttpPost]
        public ActionResult Alterar(Models.ModelLivros l)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            business.Alterar(l);

            return RedirectToAction("Listar");
        }


        public ActionResult Deletar(int id)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            Models.ModelLivros livros = business.Filtrar(id);

            return View();
        }
        [HttpPost]
        public ActionResult Deletar(int id,Models.ModelLivros l)
        {
            Business.LivroBusiness business = new Business.LivroBusiness();
            business.Deletar(id);

            return RedirectToAction("Listar");
        }
    }
}