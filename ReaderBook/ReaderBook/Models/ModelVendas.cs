﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaderBook.Models
{
    public class ModelVendas
    {
        public int idvendas { get; set; }
        public string cliente { get; set; }
        public DateTime dia { get; set; }
        public int qtd_vendas { get; set; }
        public decimal valor { get; set; }
        public int idLivro { get; set; }

}
}