﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaderBook.Models
{
    public class ModelLivros
    {
        public int idLivro { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public string genero { get; set; }
        public int qtd { get; set; }
        public decimal preco { get; set; }

    }
}