﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReaderBook.Startup))]
namespace ReaderBook
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
